#

HOME=/home/git
if [ "x$1" = 'xstart' ]; then
   if [ "x$2" = 'x-w' ]; then
     rxvt -geometry 128x18 -bg black -fg violet -name gitea -n GITea -title "gitead" -e sudo -u git \
     env GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini &
     sleep 3
   else
   echo 'type "^A^D" to detach'
   screen -mS GITEAD sudo -u git env GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini
   fi
fi
if [ "x$1" = 'xstop' ]; then
   echo "info: stopping gitead..."
   sudo -u git pkill -x -TERM gitea
   sleep 3
fi

echo http://gitea.localhost:3000/michelc
