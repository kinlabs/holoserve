#

if echo "X$1" | grep -q '^X[A-Z][A-Z0-9]*$'; then
#  locate -r /blocks/X3$ # QmUNLLsPACCz1vLxQVkXqqLX5R1X345qqfHbsf67hvA3Nn
#  locate -r /blocks/QV$ # zQmdfTbBqBPQ7VNxZEYEj14VmRuZBkqFbiwReogJgS1zR1n

#  locate -r /blocks/DZ$ # QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH
#  locate -r /blocks/PD$ # z6CfPsNrajGLLoNHWshz5fm6JwY2HBYLAyTARUUwwhWe
#  locate -r /blocks/OS$ # zdpuAqSpFsrnd3BjUfnrNPLK2zjQa3kPh5Jwpi5s7VZWRkmqj
#  locate -r /blocks/4H$ # QmToXH48SuWej4qQEnttZPJYqd3nomx7preEew4Qs12pun (ready)
#  locate -r /blocks/QC$ # QmbXBAKDgbhE8HkGuEF4FuQQJej2mxqXtYSMsBPuJDqgjq (ipfs)
  IPFS_REPO=$(locate -n 10 -r /$1/blocks/X3$ | tail -1)
  IPFS_REPO=${IPFS_REPO%/$1/blocks/X3}
  IPFS_REPO=$(locate -n 10 -r /$1/blocks/DZ$ | tail -1)
  IPFS_REPO=${IPFS_REPO%/$1/blocks/DZ}
  echo "IPFS_REPO: $IPFS_REPO"
  if [ "x$IPFS_REPO" == 'x' ]; then
    export IPFS_PATH=/media/IPFS/$1
  else
    export IPFS_PATH=$IPFS_REPO/$1
  fi
  echo "IPFS_PATH: $IPFS_PATH"
  shift
else
  export IPFS_PATH=${IPFS_PATH:-$HOME/.ipfs}
fi

NAME=$(cat $IPFS_PATH/name)
if [ "x$NAME" = 'x' ]; then
  echo ${IPFS_PATH##*/} >> $IPFS_PATH/name
  vim -g $IPFS_PATH/name
fi
echo "name: ${NAME:-${IPFS_PATH##*/}}"
if [ "x$1" = 'xstart' ]; then
   if [ "x$2" = 'x-w' ]; then
     GW=$(cat $IPFS_PATH/config | xjson Addresses.Gateway)
     pp=$(echo $GW|cut -d'/' -f5)
     name=$(cat $IPFS_PATH/config | xjson Identity.PeerID | perl -S fullname.pl)
     rxvt -geometry 128x18 -bg black -fg lightyellow -name IPFS -n "$pp" -title "$NAME:ipfs daemon:$pp (default) ~ $name" -e ipfs daemon &
     sleep 7
   else 
   # start screen in "detached" mode.
   screen -dmS $NAME ipfs daemon
   # screen -list
   # screen -r {{sessionname}} # to reattach
   # CTRL-A CTRL-D to detach ...
   sleep 7
   fi
fi
if [ "x$1" = 'xstatus' ]; then
   echo IPFS_PATH: $IPFS_PATH
   echo "addrs: |-"
   ipfs swarm addrs local | sed -e 's/^/  /'
   ipfs repo stat
   echo "ipfs: $(ipfs cat zz38RTafUtxY)"
fi
if [ "x$1" = 'xstop' ]; then
   #screen -dmS IPFS ipfs shutdown
   echo "info: stopping ipfs..."
   ipfs shutdown
   sleep 3
   if ipfs shutdown 1>/dev/null 2>&1; then
   echo "info: ipfs is down"
   fi
  
fi
gw="$(ipfs config Addresses.Gateway)"
gw_port=$(echo $gw | cut -d'/' -f 5-)
gw_host=$(echo $gw | cut -d'/' -f 3)
echo "gw: $gw"
echo "api: $(ipfs config Addresses.API)"
url="http://$gw_host:$gw_port/ipfs/zz38RTafUtxY"
echo "ipfs: $(curl -s $url)"
