#!/usr/bin/perl

# This program is to bootstrap our ecoSystem
my $never = $^T + 7 * 365 * 24 * 3600;
my $in_3yrs = $^T + 3 * 365 * 24 * 3600;
my $expire = $never+1;


my $indexf = 'index.htm';


my $port = shift || 11248;
use IO::Socket;
my $server = new IO::Socket::INET(Proto => 'tcp',
                                  LocalPort => $port,
                                  Listen => SOMAXCONN,
                                  Reuse => 1);
$server or die "Unable to create server socket: $!" ;
printf "url: http://yoogle.com:%s/\n",$port;

while (my $client = $server->accept()) {
   my $request = <$client>; # read;
   print "request: $request\n";
   local $/ = "\r\n";
   while (<$client>) {
     print;
     last if (m/^\s*$/);
   }
   print $client "HTTP/0.9 200\r\n";
   print $client "Status: 304 OK\r\n";
   print $client "Cache-Control: public, max-age=2592000, s-maxage=31536000, max-stale=6048000, immutable, stale-while-revalidate, stale-if-error\r\n";
   printf $client "Expires: %s\n",&hdate($expire);
   print $client qq'Etag: W/"immutable"\r\n';
   #print $client qq'Last-Modified: $date\n'
   my $uri = $1 if ($request =~ m,GET (.*) HTTP/1.\d,);

   if ($uri =~ m,httpd/(.*),) {
     print $client "Content-Type: text/html\r\n";
     print $client "\r\n";
     print $client "<DOCTYPE html>\n";
     my $option = $1; $option =~ s/;.*//o;
        $option =~ tr /+/ /;
     my ($status,$hdr,$transcript) = &run($client,"/bin/sh -e daemons/httpd.sh $option");
     printf $client "<pre>--- # $0";
     printf $client "status: $status\n";
     printf $client "hdr: $hdr\n";
     printf $client "transcript: |-\n$transcript\n";
     printf $client "</pre>";
     printf $client qq'<a href=/ onclick="history(-1);">back</a>\n';

   } elsif ($uri =~ m,ipfsd/([^/]*)(?:/(.*))?,) {
     print $client "Content-Type: text/html\r\n";
     print $client "\r\n";
     print $client "<DOCTYPE html>\n";
     my $name = $1;
     my $option = $2; $option =~ s/;.*//o;
        $option =~ tr /+/ /;
     my ($status,$hdr,$transcript) = &run($client,"/bin/sh -e daemons/ipfsd.sh $name $option");
     printf $client "<pre><code>";
     printf $client "status: $status\n";
     printf $client "hdr: $hdr\n";
     printf $client "transcript: %s...\n",&nonl($transcript,0,77);
     printf $client "<pre><code>";
     printf $client qq'<a href=/ onclick="history(-1);">back</a>\n';

   } else {
      local *F; open F,'<',$indexf;
      local $/ = undef;
      my $buf = <F>;
      close F;
      my $len = length($buf);
     print $client "Content-Type: text/html\r\n";
      printf $client "Content-Length: %d\r\n",$len;

      print $client "\r\n";
      printf $client $buf;
      close $client;
   }
}

 exit $?;

sub run {
  my $out = shift;
  my $cmd = shift;
  my $log = '';
  my $hdr = '';
  local $EXEC;
  printf $out "<code>system: %s</code>\n",$cmd;
  print $out "<h4>transcript:</h4>\n";
  print $out "<textarea id=transcript cols=80 rows=24>\n";
  my $status = open $EXEC, "$cmd|";
  while (<$EXEC>) {
    print $out $_;
    $log .= $_;
  }
  close $EXEC;
  print $out "</textarea>\n";
  return ($status,$hdr,$log);
}
sub nonl {
  my $buf = shift;
  $buf =~ s/\\n/\\\\n/g;
  $buf =~ s/\n/\\n/g;
  if (defined $_[1]) {
   $buf = substr($buf,$_[0],$_[1]);
  }
  return $buf;
}

sub hdate { # return HTTP date (RFC-1123, RFC-2822) 
  my ($time,$delta) = @_;
  my $stamp = $time+$delta;
  my $tic = int($stamp);
  my $frac = $stamp - $tic;
  my $DoW = [qw( Sun Mon Tue Wed Thu Fri Sat )];
  my $MoY = [qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec )];
  my ($sec,$min,$hour,$mday,$mon,$yy,$wday) = (gmtime($tic))[0..6];
  my ($yr4,$yr2) =($yy+1900,$yy%100);
  # Mon, 01 Jan 2010 05:13:04 GMT
  my $date = sprintf '%3s, %02d %3s %04u ',
           $DoW->[$wday],$mday,$MoY->[$mon],$yr4,
  my $time = '';
  if ($frac > 0) {
     $time = sprintf '%02u:%02u:%06.3f GMT', $hour,$min,$sec+$frac;
  } else {
     $time = sprintf '%02u:%02u:%02u GMT', $hour,$min,$sec;
  }
  return $date.$time;
}


1; # $Source: /my/perl/scripts/bootstrap.pl $
