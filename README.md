---
---

## HTTP servers for holoVerse


* CivetWeb (http://yoogle.com:8088)
* 
* tiny-web-server.pl
* miniserver.pl
* simple.pl
* httpi-1.7.2
* Net::Server (can use ssl via stunnel)
* HTTP::Daemon
* [Cro:HTTP::Server][cro] (Perl6 Raku)


[cro]: https://cro.services/
