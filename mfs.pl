#!/usr/bin/perl

my $dbug = 1;
# MFS2IPFS :
# - simple proxy server that returns the ipfs address of any mfs path

if ($dbug) {
 print "info: loading YAML module\n";
 eval "use YAML::Syck qw(Dump);"
}

my $port = shift || 11248;

use IO::Socket;
my $server = new IO::Socket::INET(Proto => 'tcp',
                                  LocalPort => $port,
                                  Listen => SOMAXCONN,
                                  Reuse => 1);
if ($server or die "Unable to create server socket: $!") {
 print "server: listening to port $port\n";
}
while (my $client = $server->accept()) {
   my $request = <$client>; # read;
   my $serveraddr = join'.',unpack'C4',$client->sockaddr();
   my $remoteaddr = join'.',unpack'C4',$client->peeraddr();

   printf ">request: %s\n",$request;
   my $response = &handle($request);
   if ($dbug) {
     print "HTTP/0.9 100\n";
     printf "<response: %s.\n",$response;
     printf $client "%s.\n",$response;
   } else {
     printf $client $response;
   }
   close $client;
}

exit $?;

sub handle {
  my $req = shift;
  my $hdate = &hdate(time);
  my ($method,$uri,$version) = ($req =~ m,([A-Z]+)\s(.*)\sHTTP/(\d\.\d+),o);
  print "method: $method\n";
  print "uri: $uri\n";
  print "version: $version\n";
  my ($url,$query);
  if ($uri =~ m/\?/) {
     ($url,$query) = split('\?',$uri);
  } else {
     $url = $uri;
  }
  # ---------------------------------------------------------------------------
  if ($url =~ m,^/mfs(/.*),) {
    my $mfs = $1;
    my ($base,$path);
    if ($mfs =~ m,(.*)/\./(.*)$,) { # explicit /./
      $base = $1; $path = $2;
    } elsif ($mfs =~ m,(.*)/(.git/.*)?$,) { # repo/.git/
      $base = $1; $path = $2;
    } elsif ($mfs =~ m,(.*\.git)/(.*)?$,) { # repo.git
      $base = $1; $path = $2;
    } elsif ($mfs =~ m,(.*)/([^/]*)?$,) { # dirname/basename
      $base = $1;
      $path = $2;
    }
    print "base: $base\n";
    print "path: $path\n";
    my $mh = &ipms_get_api('files/stat',$base,'&hash=1');
    if ($dbug) {
      printf "mh: %s.\n",Dump($mh);
    }
    
    my $status = '5xx';
    my $header;
    my $body;
    if (defined $mh->{Hash}) {
      $ipath = '/ipfs/'.$mh->{Hash};
      if ($mfs =~ /\.git/) {
        $status = '308';  # Permanent Redirect
      } else {
        $status = '302'; # Found temporarily moved
      }
      #$header = "Status: 302 Found\r\n"; # tempo
      #$header = "Status: 301 Moved\r\n"; # permanent
      #$header = "Status: 307 Temporary Redirect\r\n"; 
      #$header = "Status: 308 Permanently Redirect\r\n"; 
      $header = sprintf "Status: %s REDIRECT\r\n",$status; 
      $header .= 'Date: '.$hdate."\r\n"; 
      $header .= 'Location: http://127.0.0.1:8080'.$ipath.'/'.$path.'?'.$query."\r\n";
      $body = '';
    } else {
      $status = '218'; # This is fine !
      $header = "Content-Type: application/yaml\r\n"; 
      $header = "Content-Type: text/plain\r\n"; 
      $header .= 'Date: '.$hdate."\r\n"; 

      $body = sprintf "--- # %s\n",$0;
      $body .= sprintf "tics: %s\n",$^T;
      $body .= sprintf "remote_addr: %s\n",$remoteaddr;
      $body .= sprintf "server_addr: %s\n",$serveraddr;
      $body .= sprintf "request: %s\n",$request;
      if ($dbug) {
        $body .= "mh: %s...\n",Dump($mh);
      }
    }
    my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
    $response .= $header;
    my $length = length($body);
    $response .= sprintf"Length: %d\r\n\r\n",$length;
    $response .= $body if ($length);
    return $response;
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m,^/ipfs(/.*),o) { # reverse proxy w/ caching
    my $ipath = $1;

    my $status = '305'; # use proxy
    my $response = sprintf "HTTP/0.9 %s\r\n",$status; 
    $response .= sprintf"Length: %d\r\n\r\n",0;

    return "HTTP/1.0 304 Not Modified\r\n\r\n";
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m,^/kgi(/.*),o) { # KIN Gateway Interface
    my $response = "HTTP/0.9 501 Not Yet Available\r\n\r\n";
    $response .= "info: KGI Not yes available\nurl: $url\n";
    return $response;
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m,^/ipms(/.*),o) {
    my $response = "HTTP/0.9 501 Not Yet Available\r\n\r\n";
    $response .= "info: IPMS Not yes available\nurl: $url\n";
    return $response;
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m,^/ipns(/.*),o) { # IPNS ...
    return "HTTP/1.1 200\r\n\r\nLocation: https://gateway.ipfs.io/ipns/$1\r\n\r\n";
  } elsif ($url =~ m,^/p2p(/.*),o) {
    my $response = "HTTP/1.0 200\r\n\r\n";
    $response .= "url: $url\n";
    return $response;
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m,^/$,) { # ROOT (static)
    my $status = '200'; # OK
    my $header = "Content-Type: text/plain\r\n"; 
       $header .= 'Date: '.$hdate."\r\n"; 
    my $body = sprintf "%s: HELO MFS SERVER\n",$0;
    my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
    $response .= $header;
    my $length = length($body);
    $response .= sprintf"Length: %d\r\n\r\n",$length;
    $response .= $body if ($length);
    return $response;
  } elsif ($url =~ m,^/fav.*,) { # favicon
      $status = '308';
      my $favicon = $url; $favicon =~ s/\.ico$/.png/;
      $ipath = '/ipfs/QmWkcyGVZCJaic8FCSqQpbVsU9vJFkbKKwJNmg3FxroFx1';
      $header = sprintf "Status: %s Permanently Moved\r\n",$status; 
      $header .= "Content-Type: image/png\r\n"; 
      $header .= 'Date: '.$hdate."\r\n"; 
      $header .= 'Location: http://127.0.0.1:8080'.$ipath.$favicon."\r\n";
      $body = '';
      my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
      $response .= $header;
      my $length = length($body);
      $response .= sprintf"Length: %d\r\n\r\n",$length;
      $response .= $body if ($length);
      return $response;
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m/204/) {
    return "HTTP/1.0 204\r\n\r\n"; # no content
  } elsif ($url =~ m/teapot/) {
    return "HTTP/1.2 418\r\nStatus: 418 I am a Teapot\r\nLocation: https://www.google.com/teapot\r\n\r\n";
  } elsif ($url =~ m/420/) {
    return "HTTP/1.2 420\r\nStatus: 420 Enhance Your Calm\r\n\r\n";
  } elsif ($url =~ m/444/) {
    return "HTTP/1.2 444\r\nStatus: 444 No Response\r\n\r\n";
  # ---------------------------------------------------------------------------
  } elsif ($url =~ m/5xx/) {
    return "HTTP/1.2 521\r\nStatus: 521 Web Server Is Down\r\n\r\n";
  # ---------------------------------------------------------------------------
  } else {
    my $route = LoadFile('route.yml');
    foreach my $pat (keys %$route) {
      if ($url =~ m/$pat/) {
        # TBD
      }
    }
    if ($url =~ m,http://127\.,) { # local site ... proxy it !
      $status = '100';
      my $response = &get_http_content($url);
      return $response;
    } elsif ($url =~ m/https?:/) { # moved to external site ...
      $status = '302';
      my $header = sprintf "Status: %s Moved\r\n",$status; 
      my $header .= 'Location: '.$url."\r\n";

      my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
      $response .= $header;
      $response .= sprintf"Length: %d\r\n\r\n",0;
      return $response;

    } elsif ($url =~ m,files:/(.*),) { # mfs path
      my $mpath = $1;
      my $status = '200';
      my $header = sprintf "Status: %s Found\r\n",$status; 
      my $content = &get_mfs_content($mpath);
      my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
         $response .= $header;
         $response .= &build_response($content);
      return $response;
      
    } elsif ($url =~ m,file:/(.*),) {
      my $path = $1;
      my $status = '100';
      my $header = sprintf "Status: %s Found\r\n",$status; 
      my $body = &get_file_content($path);
      my $response = sprintf "HTTP/1.0 %s\r\n",$status; 
         $response .= $header;
         $response .= &build_response($body);
      return $response;
    } else {
      return "HTTP/0.9 410\r\n\r\n"; # gone 
    }
  }
  # ---------------------------------------------------------------------------
}

exit $?;

# -----------------------------------------------------
sub build_response {
   my $body = shift;
   my $length = length($body);
   my $mime = &get_mime($body);
   my $header .= sprintf "Content-Type: %s\r\n",$mime; 
      $header .= 'Date: '.$hdate."\r\n"; 
   my $response = $header;
   $response .= sprintf"Length: %d\r\n\r\n",$length;
   $response .= $body if ($length);
   return $response;
}
# -----------------------------------------------------
sub get_mfs_content {
  my $mpath = shift;
  # test if file exists
  my $mh = &ipms_get_api('files/stat',$mpath,'&hashonly=1');;
  printf "--- # mk %s...\n",Dump($mh); 
  my $content = &ipms_get_api('files/read',$mpath,'');;
  return $content;
}
# -----------------------------------------------------

sub ipms_get_api {
   my $api_url;
   my ($apihost,$apiport) = &get_apihostport();
   my $api_url = sprintf'http://%s:%s/api/v0/%%s?arg=%%s%%s',$apihost,$apiport;
   my $url = sprintf $api_url,@_; # failed -w flag !
#  printf "X-api-url: %s\n",$url;
   my $content = '';
   use LWP::UserAgent qw();
   my $ua = LWP::UserAgent->new();
   my $resp = $ua->get($url);
   if ($resp->is_success) {
#     printf "X-Status: %s\n",$resp->status_line;
      $content = $resp->decoded_content;
   } else { # error ... 
      print "[33m";
      printf "X-api-url: %s\n",$url;
      print "[31m";
      printf "Status: %s\n",$resp->status_line;
      $content = $resp->decoded_content;
      local $/ = "\n";
      chomp($content);
      print "[32m";
      printf "Content: %s\n",$content;
      print "[0m";
   }
   if ($_[0] =~ m{^(?:cat|files/read)}) {
     return $content;
   } elsif ($content =~ m/^{/) { # }
      use JSON qw(decode_json);
      my $json = &decode_json($content);
      return $json;
   } else {
      printf "Content: %s\n",$content if $dbug;
      if (0) {
         $content =~ s/"/\\"/g;
         $content =~ s/\x0a/\\n/g;
         $content = sprintf'{"content":"%s"}',$content;
      }
      return $content;
   }
}
# -----------------------------------------------------
sub get_mime {
  my $content = shift;
  use File::Type;
  my $ft = File::Type->new();
  my $mime = $ft->checktype_contents($data);
  return $mime;
}
sub get_ext {
  my $file = shift;
  my $ext = $1 if ($file =~ m/\.([^\.]+)/);
  if (! $ext) {
    my %ext = (
    text => 'txt',
    'application/octet-stream' => 'blob',
    'application/x-perl' => 'pl'
    );
    my $type = &get_type($file);
    if (exists $ext{$type}) {
       $ext = $ext{$type};
    } else {
      $ext = ($type =~ m'/(?:x-)?(\w+)') ? $1 : 'ukn';
    }
  }
  return $ext;
}
sub get_type { # to be expended with some AI and magic ...
  my $file = shift;
  use File::Type;
  my $ft = File::Type->new();
  my $type = $ft->checktype_filename($file);
  if ($type eq 'application/octet-stream') {
    my $p = rindex $file,'.';
    if ($p>0) {
     $type = 'files/'.substr($file,$p+1); # use the extension
    }
  }
  return $type;
}
# -----------------------------------------------------
sub get_file_content {
  my $file = shift;
  if (! -e $file && -e $file.'.html') {
    $file .= '.html';
  }
  if (-d $file) {
    if (-f $file.'/index.html') {
      $file = $file.'/index.html';
    } elsif (-f $file.'/200.html') {
      $file .= '200.html'; # OK
    } elsif (-f $file.'/401.html') {
      $file .= '/401.html'; # unauthorized
    } elsif (-f $file.'/403.html') {
      $file .= '/403.html'; # forbidden
    } else {
      $file = '400.html'; # bad request
    }
  }
  if (! -e $file) {
    $file =~ s,[^/]*$,200.html,;
    if (! -e $file) {
     $file =~ s,[^/]*$,404.html,;
    } else {
     $file = '410.html'; # gone
    }
  }
  my $body;
  if (-f $file) {
    local *F; open F,'<',$file or warn "$file: ".$!;
    binmode(F) unless ($file =~ /.txt/);
    local $/ = undef; my $body = <F>; close F;
  } else {
      $file = '200.html'; # OK
      local *F; open F,'<',$file;
      local $/ = undef; my $body = <F>; close F;
  }
  return $body;
}
# -----------------------------------------------------
sub get_apihostport { # need to use referer for detect "core"
  my $IPFS_PATH = $ENV{IPFS_PATH} || $ENV{HOME}.'/.ipfs';
  my $conff = $IPFS_PATH . '/config';
  local *CFG; open CFG,'<',$conff or warn $!;
  local $/ = undef; my $buf = <CFG>; close CFG;
  use JSON qw(decode_json);
  my $json = decode_json($buf);
  my $apiaddr = $json->{Addresses}{API};
  my (undef,undef,$apihost,undef,$apiport) = split'/',$apiaddr,5;
      $apihost = '127.0.0.1' if ($apihost eq '0.0.0.0');
  return ($apihost,$apiport);
}
# -----------------------------------------------------
sub hdate { # return HTTP date (RFC-1123, RFC-2822) 
  my ($time,$delta) = @_;
  my $stamp = $time+$delta;
  my $tic = int($stamp);
  #my $ms = ($stamp - $tic)*1000;
  my $DoW = [qw( Sun Mon Tue Wed Thu Fri Sat )];
  my $MoY = [qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec )];
  my ($sec,$min,$hour,$mday,$mon,$yy,$wday) = (gmtime($tic))[0..6];
  my ($yr4,$yr2) =($yy+1900,$yy%100);

  # Mon, 01 Jan 2010 00:00:00 GMT
  my $date = sprintf '%3s, %02d %3s %04u %02u:%02u:%02u GMT',
             $DoW->[$wday],$mday,$MoY->[$mon],$yr4, $hour,$min,$sec;
  return $date;
}
# -----------------------------------------------------



1; # $Source: /my/perl/scripts/mfs.pl$

