#!/usr/bin/perl

# simpler server that return the ip address of the client ...

printf "--- # %s\n",$0;
my $port = shift || 19841;
printf "port: %s\n",$port;
my $dynaddr = '65.19.143.6'; # server to get "public ip"

use IO::Socket;
# -------------------------------------------------
# making a connectionto a.root-servers.net
# A side-effect of making a socket connection is that our IP address
# is available from the 'sockhost' method
my $socket = IO::Socket::INET->new(
        Proto       => 'udp',
        PeerAddr    => '198.41.0.4', # a.root-servers.net
        PeerPort    => '53', # DNS
);
my $serverhost = (defined $socket) ? $socket->sockhost : '0.0.0.0';
close $socket;
printf "serverhost: %s\n",$serverhost;
# -------------------------------------------------
# make connection to dynsm.ml
$socket = IO::Socket::INET->new(
        Proto       => 'tcp',
        PeerAddr    => $dynaddr,
        PeerPort    => '80', # https
);
print $socket "GET /cgi-bin/remote_addr.pl HTTP/1.0\r\n";
print $socket "Host: dynsm.ml\r\n";
print $socket "\r\n";
local $/ = "\n";
do 1 while (<$socket> ne "\r\n");
$serveraddr = <$socket>;
close $socket;
printf "serveraddr: %s\n",$serveraddr;
printf "url: http://127.0.0.1:%s\n",$port;
# -------------------------------------------------
#
# -------------------------------------------------
my $server = new IO::Socket::INET(Proto => 'tcp',
                                  LocalPort => $port,
                                  Listen => SOMAXCONN,
                                  Reuse => 1);
$server or die "Unable to create server socket: $!" ;
while (my $client = $server->accept()) {
   my $sink = <$client>; # read;
   print $client "HTTP/0.9 200\r\n"; 
   print $client "Content-Type: text/plain\r\n"; 
   print $client "\r\n";
   print $client $^T,"\n";
   print $client $serverhost,"\n";
   print $client $serveraddr,"\n";
   close $client;
}

exit $?;

1;


